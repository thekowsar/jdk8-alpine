FROM alpine:latest

MAINTAINER Kowsar thekowsar@gmail.com

RUN apk update && apk --update add fontconfig ttf-dejavu
RUN apk add --no-cache tzdata
ENV TZ Asia/Dhaka

RUN apk add openjdk8
ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk
ENV PATH $PATH:$JAVA_HOME/bin
RUN java -version

# docker build --tag=jdk8-alpine:1.1 .
# docker tag jdk8-alpine:1.1 kowsar/jdk8-alpine:1.1
# docker push kowsar/jdk8-alpine:1.1
# docker run -dt --name "jdk8-alpine" kowsar/jdk8-alpine:1.1 /bin/sh
# docker exec -it 5849625079f6 /bin/sh
# exit